# Vivian OpenXR Test Project

## Description
This is a test project for Open XR based interaction for the Vivian Framework with a focus on VR. It allows displaying virtual prototypes using the Vivian Core and interacting with them using an XR based device, such as the Meta Quest or Windows mixed reality headsets. For this, it utilizes the Vivian OpenXR Interaction package.

## Installation
This project is a Unity project. You may check it out and open it with a matching Unity editor.

## Usage
Depending on where you want to run the project, you may have to change the platform. Currently, it is configured for Android to be executable on the Meta Quest 2.

## Support
In case of issues, just contact the authors of the component.

## Authors
Patrick Harms, Nuremberg Institute of Technology

## License
This project is licensed open source using the Apache 2.0 license.

## Project status
There is not much to be done about this package, only in case Unity or its Open XR Interaction implementation change significantly.